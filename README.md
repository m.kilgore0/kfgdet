# KleinFourGroup's Directory Encryption Tool

Is a password-protected zip file insecure?  I don't know, but it sure feels that way.

What about a gpg-encrypted zip file?  Definitely secure, but what if the NSA/FSB/Mossad comes after me!?

How's a gpg-encrypted zip file where each file in the zip is gpg-encrypted?  Meh, too inconvenient.